# Scrapping for sales

Dummy web scraper made for discovering graphic cards into a price range in [PCcomponentes](https://www.pccomponentes.com/) and report them to a Telegram bot.

## Installation
⚠ Requires curl.

```bash
git clone https://gitlab.com/mor88888888/scrapping-for-sales.git
pip3 install numpy
```

## Usage
First of all, edit the file to set, at least, the following variables:
* price_min
* price_max
* telegram_token
* chat_id

Then, execute it:

```bash
python3 pccom-scrapper.py
```

## Support & Contributing
I you find any bug, you have a suggestion or you don't know how works a specific tool, let me know:

🐛 [Add issue](https://gitlab.com/mor88888888/scrapping-for-sales/-/issues/new)

📬 mor8@protonmail.com

## Roadmap 🚧
* Use BeautifulSoup instead of curl from the system.
* Ask for parameters, not in code.
* **Bypass Cloudfare protection [CRITICAL]**.

## Authors and acknowledgment
Author: @mor88888888

## Project status
🗂 This project is archived.

❌ It isn't working currently because of Cloudfare protection against bots.

![https://ibb.co/4tc46qD](https://i.ibb.co/XL9FhQd/2022-05-03-17-51.png)
