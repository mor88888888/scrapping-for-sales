####################################
# Title: Scraper for pccomponentes
# Author: mor88888888
# Dependencies: curl, grep, requests
####################################

import requests
import re
import sys
import os
import time
import numpy as np
import getpass
#from bs4 import BeautifulSoup
#import csv

# -- Vars --

# Host
protocol='https://'
host='www.pccomponentes.com'
# Price
price_min=float(325)
price_max=float(1000)
# Workspace
workspace="/home/"+getpass.getuser()+"/.local/share/scrapers/"
if not os.path.exists(workspace):
    print ("Path "+workspace+" not exist. I've created it")
    os.makedirs(workspace)
# Webpages to scrap and where to save
targets=[[protocol+host+'/tarjetas-graficas-amd',workspace+'tarjetas_amd.txt'],[protocol+host+'/tarjetas-graficas/geforce-rtx-3060-series/geforce-rtx-3070-series/gf-1660-series/gf-2060-series/gf-2070-series/grafica-nvidia',workspace+'tarjetas_nvidia.txt']]
active_cards_path=workspace+'active_graphic_cards.txt'
# Telegram
telegram_token=''
chat_id=''
send_message_telegram='https://api.telegram.org/bot'+telegram_token+'/sendMessage?'+chat_id+'&text='

# -- Functions --
def search(arr, value):
    for i in arr:
        if i == value:
            return True
    return False

def file_to_array(file_name,separator):
    with open(file_name, 'r') as f:
        output = [s for line in f.readlines() for s in line[:-1].split(separator)]
        f.close()
    return output

def write_line_to_file(file_name,line):
    if os.path.isfile(file_name):
        with open(file_name, 'a') as f:
            try:
                f.write(line)
                f.close()
                return True
            except:
                return False
    else:
        with open(file_name, 'w') as f:
            try:
                f.write(line)
                f.close()
                return True
            except:
                return False

def refresh():
    pages=[]
    for target in targets:
        os.system("curl -s "+target[0]+" | grep -o -E '<a href=\"/.*></a>' > "+target[1])
        pages.append(open(target[1], 'r'))

    # Current cards with the criteria
    # Does the file exist? REDUNDANT?
    if not os.path.isfile(active_cards_path):
        print ("File "+active_cards_path+" not exist. I've created it")
        os.system("touch "+active_cards_path)

    # Get active cards
    return [file_to_array(active_cards_path,'\n'),pages]

# -- Main function --

try:
    while True:
		# Declaring vars
        discovered_cards=[]
        repeated_cards=[]

		# Refresh data
        refresh_data=refresh()
        active_cards=refresh_data[0]
        pages=refresh_data[1]

        for page in pages:
            # Create the matrix of results
            results = []
            for line in page.readlines():
                fields = []
                # Clean and normalize rows
                line=line.replace("<a ", "")
                line=line.replace("></a>", "")
                line=line.replace("\"", "\'")
                line=line.replace("\' ", "\',")
                # Create the array of the row
                for s in line[:-1].split(','):
                    values = []
                    # Create the array of fields
                    for i in s[:-1].split('='):
                        i=i.replace("\'", "")
                        values.append(i)
                    fields.append(values)
                results.append(fields)

            # Use the matrix of results
            for result in results:
                price=0
                stock=0
                for field in result:
                    if (field[0] == "data-price"):
                        if (float(field[1]) >= price_min and float(field[1]) <= price_max):
                            price=1
                    if (field[0] == "data-stock-web"):
                        if (float(field[1]) <= 2):
                            stock=1
                # Price and stock ok in this result
                if price and stock:
                    csv_row=result[3][1]+','+result[2][1]+','+result[4][1]+','+protocol+host+result[0][1]
                    if not search(active_cards, csv_row):
                        discovered_cards.append(csv_row)
                        print("Added Graphic Card - "+csv_row)
                        requests.get(send_message_telegram+"🚨\nName: "+result[2][1]+"\nPrice: "+result[4][1]+"\nLink: "+protocol+host+result[0][1])
                    else:
                        # If the price is lower
                        # ...
                        repeated_cards.append(csv_row)
        out_cards=[]
        for active_card in active_cards:
            included = False
            for repeated_card in repeated_cards:
                if active_card.split(',')[0] == repeated_card.split(',')[0]:
                    included = True
            if not included:
                out_cards.append(active_card)
        if len(out_cards) > 0:
            # Delete cards
            for out_card in out_cards:
                active_cards.remove(out_card)
                print("Deleted Graphic Card - "+out_card)
                requests.get(send_message_telegram+"ℹ️ Deleted "+out_card.split(',')[1]+" ("+out_card.split(',')[2]+")")

        # Write new cards
        for discovered_card in discovered_cards:
            active_cards.append(discovered_card)

        # Update active_cards_path
        os.system("rm "+active_cards_path)
        for active_card in active_cards:
            write_line_to_file(active_cards_path,active_card+"\n")

        # Close opened pages
        for page in pages:
            page.close()

        # Sleep time
        time.sleep(10)

except:
    requests.get(send_message_telegram+"⚠️ ABORTED!!!")
